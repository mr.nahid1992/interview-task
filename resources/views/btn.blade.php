<div>
    <div class="col-md-6">

        <form method="POST"
              action="{{route('applicant_delete',$applicant->slug)}}"
              style="display: inline-block">
            @csrf
            <button class="btn btn-sm  red " onclick="return confirm('Are You sure')"
                    type="submit"><i class="fa fa-trash"></i>&nbsp;
            </button>
        </form>
    </div>

    <div class="col-md-6">
        <a class="btn btn-sm  success " href="{{route('applicant_edit',$applicant->slug)}}"><i class="fa fa-edit"></i>&nbsp;</a>
    </div>
</div>

