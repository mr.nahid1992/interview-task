<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>






    <!-- Chosen CSS -->
    <link rel="stylesheet" href="http://127.0.0.1:8000/chosen/chosen.min.css"/>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">




    <!-- jQuery DataTables CSS -->

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">


    <!-- Your custom app.css stylesheet -->
    <link href="http://127.0.0.1:8000/css/app.css" rel="stylesheet">


</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav me-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ms-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
<!-- jQuery library -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!-- jQuery Validation plugin -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>


<!-- Toastr CSS -->
<link href="http://127.0.0.1:8000/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
<script src="http://127.0.0.1:8000/bootstrap-toastr/toastr.min.js"></script>

<!-- jQuery DataTables JS -->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<!-- Your custom app.js script (deferred) -->
<script src="http://127.0.0.1:8000/js/app.js" defer></script>
<script src="{{ asset('chosen/chosen.jquery.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>

<script>
    $(document).ready(function () {
        $(".chosen-select").chosen({allow_single_deselect: true});
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

    });

  async  function get_district(division_id, selected_district_id = '') {
        let url = '{{route('get_district_by_division_id')}}';
        if (division_id) {
           await $.ajax({
                url: url,
                type: 'post',
                data: {
                    division_id: division_id,
                    selected_district_id: selected_district_id,
                    _token: '{{csrf_token()}}'
                }, beforeSend: function () {
                    $("#district").LoadingOverlay("show");
                },
                error: function () {
                    toastr.error("An error occoured!");
                },
                success: function (data) {
                    $('#district').chosen();
                    $('#district option').remove();
                    $('#district').append($(data));
                    $("#district").trigger("chosen:updated");

                }, complete: function () {
                    $("#district").LoadingOverlay("hide", true);
                    $('#thana').chosen();
                    $('#thana option').remove();
                    $("#thana").trigger("chosen:updated");
                }
            });
        }
    }

   async function get_thana(district_id, selected_thana_id = '') {
        let url = '{{route('get_thana_by_district_id')}}';
        if (district_id) {
         await   $.ajax({
                url: url,
                type: 'post',
                data: {
                    district_id: district_id,
                    selected_thana_id: selected_thana_id,
                    _token: '{{csrf_token()}}'
                }, beforeSend: function () {
                    $("#thana").LoadingOverlay("show");
                },
                error: function () {
                    toastr.error("An error occoured!");
                },
                success: function (data) {
                    $('#thana').chosen();
                    $('#thana option').remove();
                    $('#thana').append($(data));
                    $("#thana").trigger("chosen:updated");

                }, complete: function () {
                    $("#thana").LoadingOverlay("hide", true);
                }
            });
        }
    }

</script>
@yield('js')

@if($errors->any())
    @foreach ($errors->all() as $error)
        <script>
            let msg = '{{ $error }}';
            toastr.info(msg);
        </script>
    @endforeach
@endif

@if(session('error'))
    <script>
        let msg = '{{ session('error') }}';
        toastr.error(msg);
    </script>
@endif
@if(session('fail'))
    <script>
        let msg = '{{ session('fail') }}';
        toastr.error(msg);
    </script>
@endif
@if(session('success'))
    <script>
        let msg = '{{ session('success') }}';
        toastr.success(msg);
    </script>
@endif


</body>
</html>
