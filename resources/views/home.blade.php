@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Registration List Search Wizard') }}</div>

                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Applicant's Name</th>
                                <th><input type="text" class="form-control" id="name"></th>
                                <th>Email Address</th>
                                <th><input type="text" class="form-control" id="email"></th>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    Division :
                                    <select class=" form-control chosen-select" name="division" id="division">
                                        <option value="">Select Division</option>
                                        @foreach ($divisions as $key => $division)
                                            <option
                                                value="{{$division->id}}">{{$division->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </th>
                                <th>
                                    District: :
                                    <select class="form-control chosen-select" name="district" id="district">
                                        <option value="" disabled>Select District</option>


                                    </select>
                                </th>
                                <th>
                                    Upazila / Thana:
                                    <select class="form-control chosen-select" name="thana" id="thana">
                                        <option value="" disabled>Select Thana</option>
                                    </select>
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Registration List') }}</div>

                    <div class="card-body">
                        <table class="table table-bordered" id="applicant_list_table">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Division</th>
                                <th>District</th>
                                <th>Thana</th>
                                <th width="100px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            let table =$('#applicant_list_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('get_applicant') }}",
                    data: function (d) {
                        d.division = $('#division').val();
                        d.district = $('#district').val();
                        d.thana = $('#thana').val();
                        d.name = $('#name').val();
                        d.email = $('#email').val();

                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'applicant_division', name: 'applicant_division'},
                    {data: 'applicant_district', name: 'applicant_district'},
                    {data: 'applicant_thana', name: 'applicant_thana'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });


            $(document).on('change', '#division', function () {
                table.ajax.reload();
                let division_id = $(this).val();
                get_district(division_id);
            });

            $(document).on('change', '#district', function () {
                table.ajax.reload();
                let district_id = $(this).val();
                get_thana(district_id);
            });
            $('#thana').change(function () {
                table.ajax.reload();
            });
        });

    </script>
@endsection
