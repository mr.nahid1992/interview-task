@extends('layouts.app')

@section('content')

    <style type="text/css">
        .error {
            color: red;
        }
    </style>
    <div class="content">
        <div class="container">
            <div class="row">
                <form id="registrationForm" action="{{route('update_applicant')}}" method="post" class="form-horizontal"
                      enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="id" value="{{$applicant->id}}">
                    <table class="table table-bordered table-hover ">
                        <thead>
                        <tr>
                            <th colspan="3" class="text-center">
                                Registration Form
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">Applicant's Name</td>
                            <td colspan="2">
                                <input class="form-control" type="text" name="name" value="{{$applicant->name}}" id=""
                                       autocomplete="off">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">Email Address</td>
                            <td colspan="2">
                                <input class="form-control" type="text" name="email" id="" value="{{$applicant->email}}"
                                       autocomplete="off">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">Mailing Address</td>
                            <td colspan="2">
                                <input class="form-control" type="text" name="mailing_address"
                                       value="{{$applicant->mailing_address}}" id="" autocomplete="off"
                                >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Division :
                                <select class=" form-control chosen-select" name="division" id="division">
                                    <option value="">Select Division</option>
                                    @foreach ($divisions as $key => $division)
                                        <option {{$division->id== $applicant->division_id ?"selected":""}}
                                                value="{{$division->id}}">{{$division->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                District: :
                                <select class="form-control chosen-select" name="district" id="district">
                                    <option value="" disabled>Select District</option>


                                </select>
                            </td>
                            <td>
                                Upazila / Thana:
                                <select class="form-control chosen-select" name="thana" id="thana">
                                    <option value="" disabled>Select Thana</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Address Details</td>
                            <td colspan="2">
                                <textarea name="address_details" class="form-control"
                                          autocomplete="off">{{$applicant->address_details}}</textarea>
                            </td>
                        </tr>
                        <tr>
                            @php
                                $language=json_decode($applicant->language_proficiency);

                            @endphp
                            <td>Language Proficiency</td>
                            <td colspan="2">
                                <input type="checkbox" name="language[]"
                                       {{in_array('Bangla',$language)?'checked':''}} value="Bangla"> Bangla
                                <input type="checkbox" name="language[]"
                                       {{in_array('English',$language)?'checked':''}} value="English"> English
                                <input type="checkbox" name="language[]"
                                       {{in_array('French',$language)?'checked':''}} value="French"> French
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table class="table table-bordered educationQualification">
                                    <thead>
                                    <tr>
                                        <td colspan="5">
                                            Education Qualification
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Exam Name</th>
                                        <th>University</th>
                                        <th>Board</th>
                                        <th>Result</th>
                                        <th>Action</th>
                                    </tr>
                                    <tr>
                                        <th>
                                            <select class=" form-control chosen-select" name="" id="exam_name">
                                                <option value="" disabled selected>Select Exam</option>
                                                @foreach ($exams as $key => $exam)
                                                    <option
                                                        value="{{$exam->id}}">{{$exam->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th>
                                            <select class=" form-control chosen-select" name="" id="university_name"
                                            >
                                                <option value="" disabled selected>Select University</option>
                                                @foreach ($universities as $key => $university)
                                                    <option
                                                        value="{{$university->id}}">{{$university->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th>
                                            <select class=" form-control chosen-select" name="" id="board_name"
                                            >
                                                <option value="" disabled selected>Select Board</option>
                                                @foreach ($boards as $key => $board)
                                                    <option
                                                        value="{{$board->id}}">{{$board->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th>
                                            <input class=" form-control" type="text" name="" id="result"
                                                   autocomplete="off">
                                        </th>
                                        <th>
                                            <button type="button" class="btn btn-success" id="addButton">
                                                <i class="fa  fa-plus"></i>
                                            </button>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($applicant->education_qualifications)
                                        @foreach ($applicant->education_qualifications as $key => $value)
                                            <tr>
                                                <td>{{$value->exam->name}}
                                                    <input type="hidden" name="exam[]" value="{{$value->exam_id}}"></td>
                                                <td>{{$value->university->name}}
                                                    <input type="hidden" name="university[]"
                                                           value="{{$value->university_id}}"></td>
                                                <td>{{$value->board->name}}
                                                    <input type="hidden" name="board[]" value="{{$value->board_id}}">
                                                </td>
                                                <td>{{$value->result}}<input type="hidden" name="result[]"
                                                                             value="{{$value->result}}"></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger"
                                                            onclick="deleteRow(this)">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </td>

                        </tr>

                        <tr>
                            <td>Photo</td>
                            <td colspan="2">
                                @if($applicant->photo)
                                    <img src="{{static_asset($applicant->photo)}}" style="height: 100px;>
                                @endif

                                <input class=" form-control" type="file" name="photo" accept="image/*">
                                    (Only Allow Image)
                            </td>
                        </tr>
                        <tr>
                            <td>CV Attachment</td>
                            <td colspan="2">
                                @if($applicant->curriculum_vitae)
                                    <a href="{{static_asset($applicant->curriculum_vitae)}}" download>Click to
                                        download</a>
                                @endif
                                <input class="form-control" type="file" name="curriculum_vitae">
                                (Only Allow DOC/PDF)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Training
                            </td>
                            <td colspan="3">
                                <label>
                                    <input type="radio" name="have_training" value="1"
                                           onclick="showHide(1)" {{$applicant->have_training==1?'checked':''}}>
                                    Yes
                                </label>

                                <label>
                                    <input type="radio" name="have_training" value="0"
                                           onclick="showHide(0)" {{$applicant->have_training==0?'checked':''}}>
                                    No
                                </label>
                                <table class="table table-bordered " id="traningTable">
                                    <thead>
                                    <tr>
                                        <th>Training Name</th>
                                        <th>Training Details</th>
                                        <th>Action</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" name="training_name[]" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" name="training_details[]" class="form-control">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-primary" id="addTraningRow">Add
                                            </button>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $traning_details=json_decode($applicant->traning_details)
                                    @endphp
                                    @if($traning_details)
                                        @foreach ($traning_details as $key => $value)
                                            <tr class="dynamic-row">
                                                <td>
                                                    <input type="text" name="training_name[]" value="{{$value->training_name}}" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text" name="training_details[]" value="{{$value->training_details}}"class="form-control">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger remove-row" onclick="deleteRow(this)">Remove
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3">
                                <button type="button" class="btn btn-primary btn-lg " id="load1"
                                        data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing Order">
                                    Submit Order
                                </button>


                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </form>


            </div>
        </div>
    </div>

@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {


            // Bind the click event to the submit button
            $("#load1").click(function () {
                $("#registrationForm").submit(); // Manually trigger form submission when the button is clicked
            });

            showHide({{$applicant->have_training}});
            $('#addButton').on('click', function () {

                let examId = $('#exam_name').val();
                let examName = $("#exam_name option:selected").text();
                let universityID = $('#university_name').val();
                let universityName = $("#university_name option:selected").text();

                let boardId = $('#board_name').val();
                let boardName = $("#board_name option:selected").text();

                let result = $('#result').val();


                if (examId == null) {
                    toastr.error("Select Exam");
                } else if (universityID == null) {
                    toastr.error("Select University");
                } else if (boardId == null) {
                    toastr.error("Select Board");
                } else if (result == '') {
                    toastr.error("Give Result");
                } else {
                    let newRow = `
        <tr>
          <td>${examName}<input type="hidden" name="exam[]" value="${examId}"></td>
          <td>${universityName}<input type="hidden" name="university[]" value="${universityID}"></td>
          <td>${boardName}<input type="hidden" name="board[]" value="${boardId}"></td>
          <td>${result}<input type="hidden" name="result[]" value="${result}"></td>
          <td>
            <button type="button" class="btn btn-danger" onclick="deleteRow(this)">
              <i class="fa fa-trash"></i>
            </button>
          </td>
        </tr>
      `;
                    $('.educationQualification').find('tbody').append(newRow);
                    $('#exam_name').val('').trigger("chosen:updated");
                    $('#university_name').val('').trigger("chosen:updated");
                    $('#board_name').val('').trigger("chosen:updated");
                    $('#result').val('');
                }


            });

            $('#addTraningRow').on('click', function () {

                let newRow = `
                <tr class="dynamic-row">
                    <td>
                        <input type="text" name="training_name[]" class="form-control">
                    </td>
                    <td>
                        <input type="text" name="training_details[]" class="form-control">
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger remove-row" onclick="deleteRow(this)">Remove</button>
                    </td>
                </tr>`;

                $('#traningTable').find('tbody').append(newRow);

            });


            $('#registrationForm').validate({
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    mailing_address: {
                        required: true
                    },
                    division: {
                        required: true
                    },
                    district: {
                        required: true
                    },
                    thana: {
                        required: true
                    },
                    address_details: {
                        required: true
                    },

                    photo: {
                        required: true,
                        extension: 'jpg|jpeg|png'
                    },

                },
                messages: {
                    name: {
                        required: 'Name is required'
                    },
                    email: {
                        required: 'Email is required',
                        email: 'Please enter a valid email address'
                    },
                    mailing_address: {
                        required: 'Mailing Address is required'
                    },
                    address_details: {
                        required: 'Address Details is required'
                    },
                    photo: {
                        required: 'Photo is required',
                        extension: 'Photo must be in JPG, JPEG, or PNG format'
                    },
                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                submitHandler: function (form, event) {
                    let formData = new FormData(form);
                    let url = '{{route('update_applicant')}}'

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.error == false) {
                                window.location.href = '{{route('home')}}';
                            } else {
                                for (const field in response.data) {
                                    response.data[field].forEach(errorMessage => {
                                        toastr.error(errorMessage);
                                    });
                                }
                            }
                        },
                        error: function (xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });

                }
            });


            $.validator.addMethod("extension", function (value, element, param) {
                param = typeof param === "string" ? param.replace(/,/g, "|") : "png|jpe?g";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            }, $.validator.format("Photo must be in JPG, JPEG, or PNG format."));

            let division_id = {{$applicant->division_id}};
            let district_id = {{$applicant->district_id}};
            let thana_id = {{$applicant->thana_id}};
            get_district(division_id, district_id);
            get_thana(district_id, thana_id);

        });

        function showHide(pra) {

            //$('#traningTable tbody').html('');
            if (pra == 1) {
                $('#traningTable').show();
            } else {
                $('#traningTable').hide();
            }

        }

        function deleteRow(button) {
            $(button).closest('tr').remove();
        }

        $(document).on('change', '#division', function () {
            let division_id = $(this).val();
            get_district(division_id);
        });

        $(document).on('change', '#district', function () {
            let district_id = $(this).val();
            get_thana(district_id);
        });


    </script>
@endsection
