<?php

namespace App\Http\Controllers;

use App\Models\Applicant;
use App\Models\ApplicantEducationQualification;
use App\Models\Board;
use App\Models\Exam;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use lemonpatwari\bangladeshgeocode\Models\Division;
use DataTables;


use Illuminate\Support\Str;
use Carbon\Carbon;

use Validator;
use DB;

class HomeController extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {


        $data = array();
        $data['divisions'] = Division::all();
        return view('home', $data);
    }


    public function get_applicant(Request $request)
    {
        if ($request->ajax()) {
            $division = $request->input('division', Null);
            $district = $request->input('district', Null);
            $thana = $request->input('thana', Null);
            $name = $request->input('name', Null);
            $email = $request->input('email', Null);

            $data = Applicant::with('division', 'district', 'thana')
                ->select('applicants.*');
            if ($division) {
                $data->where('division_id', $division);
            }
            if ($district) {
                $data->where('district_id', $district);
            }
            if ($thana) {
                $data->where('thana_id', $thana);
            }

            if ($name) {
                $data->where('name', 'LIKE', "%$name%");
            }

            if ($email) {
                $data->where('email', 'LIKE', "%$email%");
            }


            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('applicant_division', function ($row) {
                    $applicant_division = $row->division != null ? $row->division->name : '';
                    return $applicant_division;
                })
                ->addColumn('applicant_district', function ($row) {
                    $applicant_district = $row->district != null ? $row->district->name : '';
                    return $applicant_district;
                })
                ->addColumn('applicant_thana', function ($row) {
                    $applicant_thana = $row->thana != null ? $row->thana->name : '';
                    return $applicant_thana;
                })
                ->addColumn('action', function ($row) {
                    return view('btn', ['applicant' => $row]);
                })->rawColumns(['status', 'action'])
                ->make(true);

        }
    }

    public function applicant_edit($slug)
    {
        $data = array();
        $data['applicant'] = Applicant::with('division',
            'district',
            'thana',
            'education_qualifications',
            'education_qualifications.exam',
            'education_qualifications.university',
            'education_qualifications.board'
        )
            ->select('applicants.*')
            ->where('slug', $slug)
            ->first();
        //dd($data['applicant']);
        $data['divisions'] = Division::all();
        $data['universities'] = University::all();
        $data['exams'] = Exam::all();
        $data['boards'] = Board::all();
        return view('applicant_edit', $data);
    }


    public function update_applicant(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:applicants,email,' . $request->id,
            'mailing_address' => 'required',
            'address_details' => 'required',
            'language' => 'required|array|min:1',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors(), 200);
        }


        $applicant_id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $mailing_address = $request->mailing_address;
        $address_details = $request->address_details;
        $language_proficiency = $request->language;
        $thana_id = $request->thana;
        $district_id = $request->district;
        $division_id = $request->division;
        $exames = $request->exam;
        $universities = $request->university;
        $boardes = $request->board;
        $resultes = $request->result;
        $have_training = $request->have_training;
        $training_names = $request->training_name;
        $training_details = $request->training_details;


        $applicant = Applicant::where('id', $applicant_id)->first();


        $applicant_image = '';
        $image = $request->file('photo');
        $folder_name = 'applicant_image';
        if ($request->has('photo')) {
            $applicant_image = file_upload($image, $applicant->photo, $folder_name);
        } else {
            $applicant_image = $applicant->photo;
        }


        $filePath = '';
        $file = $request->file('curriculum_vitae');
        $folder_name = 'curriculum_vitae';
        if ($request->has('curriculum_vitae')) {
            $filePath = file_upload($file, $applicant->curriculum_vitae, $folder_name);
        } else {
            $filePath = $applicant->curriculum_vitae;
        }


        if ($request->has('language')) {
            $language = json_encode($language_proficiency);
        } else {
            $temp = array();
            $language = json_encode($temp);
        }

        $traning_details = array();
        if ($have_training) {
            foreach ($training_names as $key => $training_name) {
                if ($training_name != "" && $training_details[$key] != '') {
                    $temp['training_name'] = $training_name;
                    $temp['training_details'] = $training_details[$key];
                    $traning_details[] = $temp;
                }
            }
        }


        try {
            DB::beginTransaction();


            $applicant->name = $name;
            $applicant->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $name)) . '-' . Str::random(5);
            $applicant->email = $email;
            $applicant->mailing_address = $mailing_address;
            $applicant->address_details = $address_details;
            $applicant->language_proficiency = $language;
            $applicant->photo = $applicant_image;
            $applicant->curriculum_vitae = $filePath;
            $applicant->thana_id = $thana_id;
            $applicant->district_id = $district_id;
            $applicant->division_id = $division_id;
            $applicant->have_training = $have_training;
            $applicant->traning_details = json_encode($traning_details);
            $applicant->save();


            ApplicantEducationQualification::where('applicant_id', $applicant->id)->delete();

            if ($exames) {
                foreach ($exames as $key => $exam) {
                    $app_edu_qua = new ApplicantEducationQualification();
                    $app_edu_qua->applicant_id = $applicant->id;
                    $app_edu_qua->exam_id = $exam;
                    $app_edu_qua->university_id = $universities[$key];
                    $app_edu_qua->board_id = $boardes[$key];
                    $app_edu_qua->result = $resultes[$key];
                    $app_edu_qua->save();

                }
            }
            DB::commit();
            $success = array();
            $success['appointment_id'] = $applicant->id;

            DB::commit();
            return $this->sendResponse($success, "Appointment Requested Successfully...!");
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->sendError('Unable to Request Appointment...!', $e->getMessage(), 422);
        }
    }


    public function destroy(Request $request, $slug)
    {

        try {
            DB::beginTransaction();
            $applicant = Applicant::where('slug', $slug)->first();
            if (File::exists(public_path($applicant->photo))) {
                File::delete(public_path($applicant->photo));
            }
            if (File::exists(public_path($applicant->curriculum_vitae))) {
                File::delete(public_path($applicant->curriculum_vitae));
            }
            $applicant_education_qualification = ApplicantEducationQualification::where('applicant_id', $applicant->id)
                ->get();

            if (!$applicant_education_qualification->isEmpty()) {
                $applicant_education_qualification->each->delete();
            }
            $applicant->delete();


            DB::commit();
            return redirect()->back()->with('success', 'Update Successfully');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('fail', $e->getMessage());
        }
    }

}
