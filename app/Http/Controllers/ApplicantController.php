<?php

namespace App\Http\Controllers;

use App\Models\Applicant;
use App\Models\ApplicantEducationQualification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use lemonpatwari\bangladeshgeocode\Models\Division;
use lemonpatwari\bangladeshgeocode\Models\District;
use lemonpatwari\bangladeshgeocode\Models\Thana;

use  App\Models\Exam;
use App\Models\University;
use App\Models\Board;
use Illuminate\Support\Str;
use Carbon\Carbon;

use Validator;
use DB;

class ApplicantController extends BaseController
{

    public function create()
    {
        $data = array();
        $data['divisions'] = Division::all();
        $data['universities'] = University::all();
        $data['exams'] = Exam::all();
        $data['boards'] = Board::all();
        return view('applicant_from', $data);
    }

    public function store(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:applicants',
            'mailing_address' => 'required',
            'address_details' => 'required',
            'district' => 'required',
            'division' => 'required',
            'thana' => 'required',
            'language' => 'required|array|min:1',
            'photo' => 'required|mimes:jpg,jpeg,png',
            'curriculum_vitae' => 'required|mimes:pdf,doc',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors(), 200);
        }


        $name = $request->name;
        $email = $request->email;
        $mailing_address = $request->mailing_address;
        $address_details = $request->address_details;
        $language_proficiency = $request->language;
        $thana_id = $request->thana;
        $district_id = $request->district;
        $division_id = $request->division;
        $exames = $request->exam;
        $universities = $request->university;
        $boardes = $request->board;
        $resultes = $request->result;
        $have_training = $request->have_training;
        $training_names = $request->training_name;
        $training_details = $request->training_details;


        $applicant_image = '';
        $image = $request->file('photo');
        $folder_name = 'applicant_image';
        if ($request->has('photo')) {
            $applicant_image = file_upload($image, $old_image = '', $folder_name);
        }


        $filePath = '';
        $file = $request->file('curriculum_vitae');
        $folder_name = 'curriculum_vitae';
        if ($request->has('curriculum_vitae')) {
            $filePath = file_upload($file, $old_image = '', $folder_name);
        }


        if ($request->has('language')) {
            $language = json_encode($language_proficiency);
        } else {
            $temp = array();
            $language = json_encode($temp);
        }

        $traning_details = array();
        if ($have_training) {
            foreach ($training_names as $key => $training_name) {
                if ($training_name != "" && $training_details[$key] != '') {
                    $temp['training_name'] = $training_name;
                    $temp['training_details'] = $training_details[$key];
                    $traning_details[] = $temp;
                }
            }
        }


        try {
            DB::beginTransaction();

            $applicant = new Applicant();
            $applicant->name = $name;
            $applicant->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $name)) . '-' . Str::random(5);
            $applicant->email = $email;
            $applicant->mailing_address = $mailing_address;
            $applicant->address_details = $address_details;
            $applicant->language_proficiency = $language;
            $applicant->photo = $applicant_image;
            $applicant->curriculum_vitae = $filePath;
            $applicant->thana_id = $thana_id;
            $applicant->district_id = $district_id;
            $applicant->division_id = $division_id;
            $applicant->have_training = $have_training;
            $applicant->traning_details = json_encode($traning_details);
            $applicant->save();

            if ($exames) {
                foreach ($exames as $key => $exam) {
                    $app_edu_qua = new ApplicantEducationQualification();
                    $app_edu_qua->applicant_id = $applicant->id;
                    $app_edu_qua->exam_id = $exam;
                    $app_edu_qua->university_id = $universities[$key];
                    $app_edu_qua->board_id = $boardes[$key];
                    $app_edu_qua->result = $resultes[$key];
                    $app_edu_qua->save();

                }
            }
            DB::commit();
            $success = array();
            $success['appointment_id'] = $applicant->id;

            DB::commit();
            return $this->sendResponse($success, "Appointment Requested Successfully...!");
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->sendError('Unable to Request Appointment...!', $e->getMessage(), 422);
        }


    }

    public function get_district_by_division_id(Request $request)
    {
        if ($request->ajax()) {
            $division_id = $request->division_id;
            $selected_district_id = $request->selected_district_id;
            $districts = District::where('division_id', $division_id)->get();
            $append = '';
            if ($districts) {
                $append = ' < option value = "" disabled selected > Select District </option > ';

                foreach ($districts as $key => $value) {
                    $selec = '';
                    if (!empty($selected_district_id) && $selected_district_id == $value->id):
                        $selec = 'selected';
                    endif;
                    $append .= ' <option ' . $selec . ' value = ' . $value->id . ' > ' . $value->name . '</option > ';
                }

            } else {
                $append .= '<option > empty!</option>';
            }
            echo $append;
        }
    }

    public function get_thana_by_district_id(Request $request)
    {
        if ($request->ajax()) {
            $district_id = $request->district_id;
            $selected_thana_id = $request->selected_thana_id;
            $thanas = Thana::where('district_id', $district_id)->get();
            $append = '';
            if ($thanas) {
                $append = ' < option value = "" disabled selected > Select Thana </option > ';

                foreach ($thanas as $key => $value) {
                    $selec = '';
                    if (!empty($selected_thana_id) && $selected_thana_id == $value->id):
                        $selec = 'selected';
                    endif;
                    $append .= ' <option ' . $selec . ' value = ' . $value->id . ' > ' . $value->name . '</option > ';
                }

            } else {
                $append .= '<option > empty!</option>';
            }
            echo $append;
        }
    }


}
