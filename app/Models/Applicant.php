<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use lemonpatwari\bangladeshgeocode\Models\District;
use lemonpatwari\bangladeshgeocode\Models\Division;
use lemonpatwari\bangladeshgeocode\Models\Thana;

class Applicant extends Model
{
    use HasFactory;

    protected $table = 'applicants';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'slug',
        'email',
        'mailing_address',
        'address_details',
        'language_proficiency',
        'photo',
        'curriculum_vitae',
        'thana_id',
        'district_id',
        'division_id',
        'have_training',
        'traning_details',
    ];
    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id');
    }
    public  function district(){
        return $this->belongsTo(District::class,'district_id');
    }
    public  function thana(){
        return $this->belongsTo(Thana::class,'thana_id');
    }

    public function education_qualifications(){
        return $this->hasMany(ApplicantEducationQualification::class,'applicant_id','id');
    }
}
