<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicantEducationQualification extends Model
{
    use HasFactory;

    protected $table = 'applicant_education_qualifications';
    protected $primaryKey = 'id';
    protected $fillable = [
        'applicant_id',
        'exam_id',
        'university_id',
        'board_id',
        'result'
    ];


    public function exam()
    {
        return $this->hasOne(Exam::class, 'id', 'exam_id');
    }
    public  function university(){
        return $this->hasOne(University::class,'id','university_id');
    }
    public  function board(){
        return $this->hasOne(Board::class,'id','board_id');
    }



}
