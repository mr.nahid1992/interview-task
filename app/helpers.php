<?php
/**
 * Created by PhpStorm.
 * User: Nahid
 * Date: 7/19/2023
 * Time: 10:24 AM
 */
use Illuminate\Support\Str;
if (!function_exists('file_upload')) {
    function file_upload($image, $old_image, $folder_name, $return_desult = true)
    {
        if (isset($image)) {

            if (File::exists(public_path($old_image))) {

                File::delete(public_path($old_image));
            }

            $name = date("d-m-Y") . "-" . time() . '-' . $image->getClientOriginalName();
            $name = str_replace(' ', '-', $name);
            //$name =preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $name));
            $image->move('upload/' . $folder_name . '/', $name);
            return 'upload/' . $folder_name . '/' . $name;
        } else {
            if ($return_desult == true) {
                return $imageName = 'default.png';
            } else {
                return null;
            }

        }

    }
}

if (!function_exists('static_asset')) {

    function static_asset($path, $secure = null)
    {//database upload remove this condition if

        return app('url')->asset($path, $secure);

    }
}
