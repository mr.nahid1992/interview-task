<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApplicantController;
use App\Http\Controllers\HomeController;

Auth::routes();
Route::get('/', [ApplicantController::class,'create']);
Route::post('/save-applicant', [ApplicantController::class,'store'])->name('save-applicant');
Route::post('/get_district_by_division_id', [ApplicantController::class,'get_district_by_division_id'])->name('get_district_by_division_id');
Route::post('/get_thana_by_district_id', [ApplicantController::class,'get_thana_by_district_id'])->name('get_thana_by_district_id');


Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/get_applicant', [HomeController::class,'get_applicant'])->name('get_applicant');
    Route::get('/applicant_edit/{slug}', [HomeController::class,'applicant_edit'])->name('applicant_edit');
    Route::post('/update_applicant', [HomeController::class,'update_applicant'])->name('update_applicant');
    Route::post('/applicant_delete/{slug}', [HomeController::class,'destroy'])->name('applicant_delete');


});





