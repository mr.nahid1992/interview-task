# interview task 

1.Clone The git repository<br>
2.Rename The .env file.<pre>cp .env.example .env</pre><br>
3.create a database and set the database details in the .env file.<br>
4.Run <pre>composer update</pre>
5.Run <pre>php artisan key:generate</pre>
6.Run <pre>php artisan migrate</pre>
7.Run <pre>php artisan db:seed</pre>
8.Run <pre>php artisan serve</pre>



