<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->string('slug',255)->nullable();
            $table->string('email',255)->unique();
            $table->longText('mailing_address')->nullable();
            $table->longText('address_details')->nullable();
            $table->json('language_proficiency',255)->nullable();
            $table->string('photo',255)->nullable();
            $table->string('curriculum_vitae',255)->nullable();
            $table->integer('division_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('thana_id')->nullable();
            $table->tinyInteger('have_training')->nullable()->default(0);
            $table->json('traning_details',255)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
