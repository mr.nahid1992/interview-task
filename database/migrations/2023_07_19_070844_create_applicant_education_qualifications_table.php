<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantEducationQualificationsTable extends Migration
{

    public function up()
    {
        Schema::create('applicant_education_qualifications', function (Blueprint $table) {
            $table->id();
            $table->integer('applicant_id');
            $table->integer('exam_id');
            $table->integer('university_id');
            $table->integer('board_id');
            $table->string('result');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('applicant_education_qualifications');
    }
}
